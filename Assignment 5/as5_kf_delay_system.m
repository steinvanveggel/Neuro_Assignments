function [Md]=as5_kf_delay_system(M,params)
% Delay arbitrary state space system by discrete delay line 
% Changes the observation matrix, such that only delayed states are
% observable. Changes A, B, C and D accordingly.
%
% Inputs:
%   - M:        State Space Model to be delayed.
%
%   - params:   Containing params.delay and params.dt
%   
%
% Outputs:
%   - Md:       Structure containing the new matrices to generate a the delayed model.
%               These are Md.a, Md.b, Md.c and Md.d for the extended versions
%               of the A, B, C and D matrices.
%               Matlab is case sensitive! Use exactly the same naming!
%
% -------------------------------------------------
% Taken from Joern Diedrichsen j.diedrichsen@bangor.ac.uk
% changed by Alexander Kuck a.kuck@utwente.nl

[A,B,C,D]=ssdata(M); 

n = size(A,1);
d = params.delay/params.dt;
u = size(B,2);
o = size(C,1);

A = [A zeros(n,n*(d-1)) zeros(n); ...
        eye(n) zeros(n,n*(d-1)) zeros(n); ...
        zeros(n*(d-1),n) eye(n*(d-1)) zeros(n*(d-1),n)];
B = [B; ...
        zeros((n*d),u)];
    
C = [zeros(o,(n*d)) C];

Md = ss(A,B,C,D);




        

