function [MArray, xPrioriArray, xPosterioArray, pPosterioArray] = as5_kf_kalman(ForwardModel,NoiseModel,params,models,data)
%% Function containing the Kalman Filter Equations
%
% Inputs:
%  ForwardModel:        Model of the internal forward model
%  NoiseModel:          Model of the true Hand with noise added
%  param:               Structure containing all the required parameters
%  models:              Structure containing all the required models
%  data:                Structure containing important data (inputs u,w and v)

% Outputs:
%  MArray:              [nSample x 2 x 2] - (Type Double) Matrix of kalman gains varying in time (4 kalman relevant gains changing over time)
%  xPrioriArray:        [nSample x 2] - (Type Double) Matrix of kalman a-priori state estimates (2 relevant estimates changing over time)
%  xPosterioArray:      [nSample x 2] - (Type Double) Matrix of kalman state estimates varying in time for each test (2 relevant estimates changing over time)
%  pPosterioArray:      [nSample x 2 x 2] - (Type Double) Matrix of estimated error covariance matrix in time (4 relevant estimates changing over time)
%
%                       Output variable values have to be stored in the
%                       corresponding array while iterating through time.
%                       e.g. MArray(k,2,2)=M(1:2,1:2);
%                       etc... etc....
%
%                       With:
%                           nSample=length(u)
%                       and
%                           k=1:nSample
%
% Written by : Alexander Kuck
% Date       : February 20 2017


%% 1) Define Matrices needed for Kalman Filter
[A,B,C,~] = ssdata(ForwardModel);

n = size(C,2); % number of states
m = size(C,1); % number of outputs

Q = zeros(n);
Q(1:2,1:2) = params.Q;

R = zeros(m);
R(1:2,1:2) = params.R;

u = data.u;
nSample = length(params.t);



%% 2) Preallocation (reserving space) of matrices to speed up calculations
MArray = NaN(nSample,n,m);
xPrioriArray = NaN(nSample,n)';
xPosterioArray = NaN(nSample,n)';
pPrioriArray = NaN(nSample,n,n);
pPosterioArray = NaN(nSample,n,n);

%% 3) Initialize Covariance and State Estimates
pPrioriArray(1,:,:) = zeros(n);
xPrioriArray(:,1) = zeros(n,1);
MArray(1,:,:) = squeeze(pPrioriArray(1,:,:))*C'*(C*squeeze(pPrioriArray(1,:,:))*C' + R)^-1;
xPosterioArray(:,1) = xPrioriArray(:,1) + squeeze(MArray(1,:,:))*(data.yNoise(1,:)' - C*xPrioriArray(:,1));
pPosterioArray(1,:,:) = squeeze(pPrioriArray(1,:,:)) - squeeze(MArray(1,:,:))*C*squeeze(pPrioriArray(1,:,:));

%% 4) Run the Kalman Filter
for k = 2:nSample
    xPrioriArray(:,k) =  A * xPosterioArray(:,k-1) + B * u(k-1);  
    pPrioriArray(k,:,:) = A * squeeze(pPosterioArray(k-1,:,:)) * A' + Q;
    MArray(k,:,:) = squeeze(pPrioriArray(k,:,:))*C'*(C*squeeze(pPrioriArray(k,:,:))*C'+R)^-1;
    xPosterioArray(:,k) = xPrioriArray(:,k) + squeeze(MArray(k,:,:))*(data.yNoise(k,:)' - C*xPrioriArray(:,k));
    pPosterioArray(k,:,:) = squeeze(pPrioriArray(k,:,:)) - squeeze(MArray(k,:,:))*C*squeeze(pPrioriArray(k,:,:));
end 

%% 5) Flip dimensions of x arrays 
xPrioriArray = xPrioriArray';
xPosterioArray = xPosterioArray';