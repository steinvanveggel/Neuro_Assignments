function F=as5_kf_movement(models,params)
% Function which generates a movement and returns the appropriate output F.
% 
% Inputs
%  param            : structure containing all the required parameters
%  models           : structure containing all the required models
%
% Output
%  F                : Force needed to accelerate the hand for 1 second and
%                     subsequently stop it. This should be a COLUMN vector  of
%                     the same length as t (dimensions: [length(t) x 1]),
%                     with each row being a new time step.
%                     Make sure the above dimensions are correct, or else
%                     you will get errors!
%
% Written by : Alexander Kuck
% Date       : February 20 2017

%% 1) Generate force vector
F = zeros(length(params.t),1); % empty force vector 
F(1:101) = params.force; % 1.5 N for the first 1 second 
F(102:end) = -params.force; % -1.5 N after 1 second

%% 2) Simulate real hand position with hand pulling back.
output = lsim(models.TrueHandIdeal,F,params.t);

%% 3) Find the moment at which the hand velocity reaches zero and put F to 0 to avoid hand pulling back
velocity = output(:,2);
index = find(velocity < 0, 1, 'first');
F(index:end) = 0;

%% 4) plot F
figure(1)
plot(params.t,F)
xlabel('Time [s]')
ylabel('Force [N]')
title('Force Perturbation')