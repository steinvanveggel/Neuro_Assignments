clear all
close all
clc

%% plotting kinetic and potential energy of walker 
output = importdata('passive_dynamic_walker_start_Outputs.sto');

data = output.data;

data = data(20000:end,:);

time = data(:,1);
pot = data(:,2);
kin = data(:,3);
total = pot + kin; 

%% find period of a step and energy per step
[pks,locs] = findpeaks(kin,time);
period = locs(3) - locs(1);

index2 = find(time==locs(3));
index1 = find(time==locs(1));
Eperstep = total(index1) - total(index2);

%% plot energies 
figure 

subplot(311)

plot(time,pot), hold on
xlim([6 10])
xline(locs(1))
xline(locs(3))
xline(locs(5))
xline(locs(7))
xline(locs(9))
xline(locs(11))

ylabel('E_{potential} (J)')
title('Potential Energy')

subplot(312)

plot(time,kin), hold on
xlim([6 10])
xline(locs(1))
xline(locs(3))
xline(locs(5))
xline(locs(7))
xline(locs(9))
xline(locs(11))

ylabel('E_{kinetic} (J)')
title('Kinetic Energy')

subplot(313)

plot(time,total), hold on
xlim([6 10])
xline(locs(1))
xline(locs(3))
xline(locs(5))
xline(locs(7))
xline(locs(9))
xline(locs(11))

ylabel('E_{sum} (J)')
xlabel('time (s)')
title('Total Dissipated Energy')



