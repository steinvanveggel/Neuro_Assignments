-- SCONE script for a high-jump measure.
function init( model )
	-- get the 'target_body' parameter from ScriptMeasure, or set to "pelvis"
	target_body_name = scone.target_body or "pelvis"
	
	-- find the body with target_body_name
	body = model:find_body( target_body_name )
	body_offset = vec3:new( 0, 1, 0 )
	
	-- global variable to keep track of highest position so far
	fitness = 0
end

function update( model )
	-- get current vertical position and velocity
	model:delta_time()
	
	local pos = body:point_pos( body_offset ).y
	
	-- to calculate average height 
	fitness = fitness + model:delta_time()*pos
	
	
	
	
	-- stop simulation if model com is too low
	if model:com_pos().y < 0.5 then
		
		return true -- terminate the simulation
	else
		return false -- keep simulating
	end
end

function result( model )
	-- this is called at the end of the simulation
	
	
	fitness = fitness/model:time()
	
	return fitness
end

function store_data( frame )
	-- store some values for analysis
	frame:set_value( "fitness", fitness )
end
