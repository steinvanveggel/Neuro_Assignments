function [R,origin] = local_shank(MLP,MMP,LM,MM)
L = size(MLP,2); % number of time samples, 191
for i=1:L
    MLP_i = MLP(:,i);
    MMP_i = MMP(:,i);
    LM_i = LM(:,i);
    MM_i = MM(:,i);

    origin(:,i) = (MLP_i + MMP_i + LM_i + MM_i)/4; % origin in COM of markers
    top_joint = (MLP_i + MMP_i)/2; % COM of 2 top markers to define middle of joint
    x_axis = top_joint - origin(:,i); % x axis through middle of joint
    x_axis = x_axis/norm(x_axis); % make unit vector

    y_axis = cross(MLP_i - origin(:,i),x_axis); % MLP is in plane, so y axis perpendicular to MLP and x axis
    y_axis = y_axis/norm(y_axis); % make unit vector

    z_axis = cross(x_axis,y_axis); % z axis by right hand rule 

    R(:,:,i) = [x_axis y_axis z_axis]; % fill in rotation matrix 

end
end


