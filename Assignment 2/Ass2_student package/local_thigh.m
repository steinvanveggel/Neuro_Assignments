function [R,origin] = local_thigh(FH,LE,ME)
L = size(FH,2); % number of time samples, 191

for i=1:L
    FH_i = FH(:,i);
    LE_i = LE(:,i);
    ME_i = ME(:,i);
    
    origin(:,i) = (FH_i + LE_i + ME_i)/3; % origin is COM of 3 markers
    x_axis = FH_i - origin(:,i); % direction of x axis, pointing to FH marker
    x_axis = x_axis/norm(x_axis); % make unit vector

    y_axis = cross(LE_i - origin(:,i),x_axis); % LE is in plane, so y perpendicular to x axis and LE
    y_axis = y_axis/norm(y_axis); % make unit vector

    z_axis = cross(x_axis,y_axis); % z axis by right hand rule 

    R(:,:,i) = [x_axis y_axis z_axis]; % fill in rotation matrix 

end
end


