function enablesubfunctions
%% Load the data
load('BM41040_2020-2021_ass2.mat');

%% Rotation matrices 
% call function for rotation matrix for thigh
[Rthigh,originthigh] = local_thigh(FH,LE,ME);
Rthigh_1 = Rthigh(:,:,1);
originthigh_1 = originthigh(:,1);
[Rshank,originshank] = local_shank(MLP,MMP,LM,MM);
Rshank_1 = Rshank(:,:,1);
originshank_1 = originshank(:,1);

% local coordinates of the thigh markers for time sample 1 
FH_local = inv(Rthigh_1)*(FH(:,1)-originthigh_1);
LE_local = inv(Rthigh_1)*(LE(:,1)-originthigh_1);
ME_local = inv(Rthigh_1)*(ME(:,1)-originthigh_1);

% local coordinates of the shank markers for time sample 1 
MLP_local = inv(Rshank_1)*(MLP(:,1)-originshank_1);
MMP_local = inv(Rshank_1)*(MMP(:,1)-originshank_1);
LM_local = inv(Rshank_1)*(LM(:,1)-originshank_1);
MM_local = inv(Rshank_1)*(MM(:,1)-originshank_1);

for i = 1:191
    joint(:,:,i)=inv(Rthigh(:,:,i))*Rshank(:,:,i);
    % extract matrix indices to obtain angles 
    y_angle(i)=asin(-joint(3,1,i));
    x_angle(i)=asin(joint(3,2,i)/cos(y_angle(i)));
    z_angle(i)=acos(joint(1,1,i)/cos(y_angle(i))); 
end 

% check first time indices 
joint_1 = joint(:,:,1)
y_1=y_angle(1)/pi*180
x_1=x_angle(1)/pi*180
z_1=z_angle(1)/pi*180

%% plotting angles 
%timevector, 191 samples and 100 Hz recording 
t=0.01:0.01:191*0.01; 
close all 
figure(2)
plot(t,z_angle), hold on
plot(t,y_angle)
plot(t,x_angle)
ylabel("angle [rad]")
xlabel("time [s]")
legend("z angle (flexion/extension)","y angle (abduction/adduction)","x angle (endo/exo rotation)")
title("Knee Joint Angles against time")

%% motion recording
figure(999)

for i_plot=1:length(GT)
    plot3_landmark([GT(:,i_plot);FH(:,i_plot);LE(:,i_plot);ME(:,i_plot);MLP(:,i_plot);MMP(:,i_plot);LM(:,i_plot);MM(:,i_plot)]) % Plot all markers during one time sample
end

% GT = greater trochanter
% FH = femoral head
% LE = lateral epicondyle
% ME = medial eipicondyle
% MLP = lateral ridge of tibia plateau (lateral condyle)
% MMP = medial ridge of tibia plateau (medial condyle)
% LM = lateral malleoulus
% MM =  medial malleolus 



%% SUBFUNCTIONS
function plot3_landmark(markers)

size(markers,1); % 24 lang 
nr_of_markers=size(markers,1)/3; % 8 markers
clr = ['b';'k';'y';'g';'c';'r';'m';'b'];
hold off

for i_marker=1 : nr_of_markers
    plot3(markers(i_marker*3-2,:),markers(i_marker*3-1,:),markers(i_marker*3,:),['*' clr(i_marker)]); % plot one marker
    %alle x y en z coordinaten achter elkaar zetten
    hold on;
end
scatter3(0,0,0, 'filled') %plot origin for clarity 

xlabel('x-axis'); ylabel('y-axis'); zlabel('z-axis');
axis equal
axis([-1600 1000 -100 300 0 900])

view(-135,20)

drawnow


