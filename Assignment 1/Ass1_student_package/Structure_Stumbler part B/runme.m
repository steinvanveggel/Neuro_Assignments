clc
clear all

%% Derive the motion equations
run stumbler_derive_symbolic.m

%% Run simulink model
sim('stumbler_simulation.mdl')

%% Plot joint torques and angles

T = U_in.signals.values;
alpha = X_out.signals.values;
t = U_in.time;
        
titles = ["\gamma_1", "\alpha_2", "\beta_2", "\gamma_2", "\gamma_3", "\gamma_4"];

figure
for i = 1:size(T,2)
    nexttile
    
    ymax = max(alpha(:,i));
    ymin = min(alpha(:,i));
    yrange = ymax - ymin;
    
    hold on
    
    plot(t,alpha(:,i), 'k')
    
    if yrange > 0.1
        ylim([ymin-0.1*yrange, ymax+0.1*yrange])
    end
    title("Joint "+titles{i})
    legend("Angles (rad)")
    xlabel('Time (s)')
    
    hold off
    
end