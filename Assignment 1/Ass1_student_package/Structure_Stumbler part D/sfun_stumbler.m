function [sys,x0,str,ts] = sfun_stumbler(t,x,u,flag)
global model
%% Define model masses, lengths, stiffness, damping and gravity
% Find reasonable values for the following variables and store them in 
% the model structure, defined globally above. 
% example: model.mstance = <your value here> %[kg]; 
% (hint; consider the dimensions (single value/vectors) of the variables)

% Masses
m_total = 70; %kg
l_total = 1.8;   %[m]

model.mthigh    = 0.1*m_total;   % mass thigh swing leg [kg]
model.mshank    = 0.0465*m_total;   % mass shank swing leg [kg]
model.mfoot     = 0.0145*m_total;   % mass foot swing leg [kg]
model.mstance   = model.mthigh + model.mshank + model.mfoot;   % mass stance leg [kg]
model.mhip      = m_total - 2*model.mstance;   % mass trunk on hip [kg

% Lengths

model.Lstance   = 0.53*l_total;% [m]
model.Lhip      = 0.191*l_total;% [m]
model.Lthigh    = 0.245*l_total;% [m]
model.Lshank    = 0.285*l_total;% [m]
model.Lfoot     = 0.152*l_total;% [m]

% Centres of gravity with respect to proximal joint
model.cgStance =  0.5*model.Lhip;% [m] %LATER NOG AANPASSEN
model.cgThigh  =  0.433*model.Lthigh;% [m]
model.cgShank  =  0.433*model.Lshank;% [m]
model.cgFoot   =  0.50*model.Lfoot;% [m]

% Joint stiffness
model.kjoint    = [0.5; 0.3; 1; 0.1; 100; 1];

% Joint damping
model.bjoint    = [0.5; 0.5; 0.5; 0.5; 1; 0.5];

% Gravity
model.g         = -9.81; % [Nm/s^2]

% % Part B: brick dimensions
xbrick          = 0;
model.bx1       = xbrick;
model.bx2       = xbrick + 0.06;
model.by1       = 0;
model.by2       = 0.10;
model.bz1       = 0.17;
model.bz2       = 0.45;
model.bbrick    = 100;
model.kbrick    = 5*1000;

%% 
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
    [sys,x0,str,ts]=mdlInitializeSizes;

  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  case 1,
    sys=mdlDerivatives(t,x,u,model);

  %%%%%%%%%%
  % Update %
  %%%%%%%%%%
  case 2,
    sys=mdlUpdate(t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
    sys=mdlOutputs(t,x,u);

  %%%%%%%%%%%%%%%%%%%%%%%
  % GetTimeOfNextVarHit %
  %%%%%%%%%%%%%%%%%%%%%%%
  case 4,
    sys=mdlGetTimeOfNextVarHit(t,x,u);

  %%%%%%%%%%%%%
  % Terminate %
  %%%%%%%%%%%%%
  case 9,
    sys=mdlTerminate(t,x,u);

  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);
end

% end sfuntmpl

%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes

% Init sizes
sizes = simsizes;
sizes.NumContStates  = 12; %continuous states     % Two times the number of generalized coordinates
sizes.NumDiscStates  = 0; %discrete states
sizes.NumOutputs     = 6;       % All joint angles
sizes.NumInputs      = 6;       % All joint torques
sizes.DirFeedthrough = 0;   %vgm is dit de flag
sizes.NumSampleTimes = 20;       % At least one sample time is needed
sys = simsizes(sizes);

% Initial angles [rad]
% Define what the initial values of the angles are
gamma1  = 25/180*pi;
alpha2  = 0/180*pi;
beta2   = 0/180*pi;
gamma2  = -50/180*pi;
gamma3  = 0/180*pi;
gamma4  = 115/180*pi;

% Initial angular velocities [rad/s]
% Define what the initial values of the angular velocities are
gamma1dot   = -90/180*pi;
alpha2dot   = 0/180*pi;
beta2dot    = 0/180*pi;
gamma2dot   = 0/180*pi;
gamma3dot   = 0/180*pi;
gamma4dot   = 0/180*pi;

% States and state derivatives
q       = [gamma1; alpha2; beta2; gamma2; gamma3; gamma4];
qdot    = [gamma1dot; alpha2dot; beta2dot; gamma2dot; gamma3dot; gamma4dot];

x0  = [q; qdot];


% str is always an empty matrix
str = [];


% initialize the array of sample times
fs  = 0.1;
ts  = [linspace(0,fs*sizes.NumSampleTimes,sizes.NumSampleTimes)' zeros(sizes.NumSampleTimes,1)];

% end mdlInitializeSizes

%
%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
function sys=mdlDerivatives(t,x,u,model)
%% Get model parameters from model structure 
% Masses
mstance     = model.mstance;     % mass stance leg [kg]
mhip        = model.mhip;        % mass trunk on hip [kg
mthigh      = model.mthigh;      % mass thigh swing leg [kg]
mshank      = model.mshank;      % mass shank swing leg [kg]
mfoot       = model.mfoot;      % mass foot swing leg [kg]

% Lengths
Lstance     = model.Lstance;    % [m]
Lhip        = model.Lhip;       % [m]
Lthigh      = model.Lthigh;     % [m]
Lshank      = model.Lshank;     % [m]
Lfoot       = model.Lfoot;      % [m]

% Centres of gravity with respect to proximal joint
cgStance    = model.cgStance;   % [m]
cgThigh     = model.cgThigh;    % [m]
cgShank     = model.cgShank;    % [m]
cgFoot      = model.cgFoot;     % [m]

% Gravity
g           = model.g;

%% Determine mass matrix
mass = diag([0 0 0 ...
            (mhip+mstance) (mhip+mstance) (mhip+mstance) ...
             0 0 0 ...
             mthigh mthigh mthigh ...
             0 0 0 ...
             mshank mshank mshank ...
             0 0 0 ...
             mfoot mfoot mfoot ...
             0 0 0]);
    
%% Get state variables from state vector x
q           = x(1:6);
qdot        = x(7:12);
gamma1      = q(1);
alpha2      = q(2);
beta2       = q(3);
gamma2      = q(4);
gamma3      = q(5);
gamma4      = q(6);
gamma1dot   = qdot(1);
alpha2dot   = qdot(2);
beta2dot    = qdot(3);
gamma2dot   = qdot(4);
gamma3dot   = qdot(5);
gamma4dot   = qdot(6);


%% Add dynamics stiffness and damping to prevent overstretching joints
% First get the stored values of passive stiffness and damping from 
% the model structure

% Passive Stiffness
kjoint      = model.kjoint;

% Passive Damping
bjoint      = model.bjoint;

% Active stiffness and damping
% Create if statement, which prevents the knee to overstretch by adapting
% the stiffness and damping
% if gamma3 > 0 % Knee overstretched
%      kjoint(5) = 10;
%      bjoint(5) = 0;
% end

%% Obtain transformation vector Ti and its derivatives Ti_k and Ti_km
symb_Ti;
symb_Ti_k;

%% Detect collision with brick
% Implement the collision with the brick and calculate the resulting force.
% Reduced force vector 
f = [0 0 0 ...
     0 (model.mhip+model.mstance)*model.g 0 ...
     0 0 0 ...
     0 model.mthigh*model.g 0 ...
     0 0 0 ...
     0 model.mshank*model.g 0 ...
     0 0 0 ...
     0 model.mfoot*model.g 0 ...
     0 0 0]';

% xjoint = [Ti(19:6:25)];
% % xvec = linspace(xjoint(1),xjoint(2),10);
% yjoint = [Ti(20:6:26)];
% % yvec = linspace(yjoint(1),yjoint(2),10);
% zjoint = [Ti(21:6:27)];
% % zvec = linspace(zjoint(1),zjoint(2),10);
% 
% 
% Tidot = Ti_k*qdot; %velocities of COM and joints
% 
% xdjoint = [Ti(19:6:25)];
% ydjoint = [Ti(20:6:26)];
% zdjoint = [Ti(21:6:27)];
% 
% testcollision = (model.bx1 < xjoint & xjoint < model.bx2).*(yjoint < model.by2).*(model.bz1 < zjoint & zjoint < model.bz2); 
% 
% if nnz(testcollision)>0
%     disp('COLLISION');
% end 

% Fbx = model.kbrick*(model.bx1-xjoint(end-1)) - model.bbrick*xdjoint(end-1);
% Fby = model.kbrick*(model.by2-yjoint(end-1)) - model.bbrick*ydjoint(end-1);
% Fbz = 0;
% f(end-8:6:end-2)= Fbx*testcollision;
% f(end-6:6:end)= Fbz*testcollision;
% f(end-7:6:end-1)= Fby*testcollision;

%% Determine state derivatives
% Calculate reduced mass matrix Mred 
Mred = Ti_k'*mass*Ti_k;

% Calculate convective acceleration 
symb_gconv;
 
%values where the spring force pulls the state back to
%so 90 degrees for the ankle
q_rest = [0 0 0 0 0 90/180*pi]'; 
 
 
Fred = Ti_k'*(f-mass*gconv) - (bjoint .* qdot) - (kjoint .* (q-q_rest)) + u;

% Calcultate derivatives qddot 
qddot = Mred\Fred;

sys = [qdot ; qddot];

% end mdlDerivatives

%
%=============================================================================
% mdlUpdate
% Handle discrete state updates, sample time hits, and major time step
% requirements.
%=============================================================================
%
function sys=mdlUpdate(t,x,u)
% Empty matrix
sys = [];
% end mdlUpdate

%
%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================
%
function sys=mdlOutputs(t,x,u)

% Create output
sys = x(1:6);

% end mdlOutputs

%
%=============================================================================
% mdlGetTimeOfNextVarHit
% Return the time of the next hit for this block.  Note that the result is
% absolute time.  Note that this function is only used when you specify a
% variable discrete-time sample time [-2 0] in the sample time array in
% mdlInitializeSizes.
%=============================================================================
%
function sys=mdlGetTimeOfNextVarHit(t,x,u)

sampleTime = 1;    %  Example, set the next hit to be one second later.
sys = t + sampleTime;

% end mdlGetTimeOfNextVarHit

%
%=============================================================================
% mdlTerminate
% Perform any end of simulation tasks.
%=============================================================================
%
function sys=mdlTerminate(t,x,u)

sys = [];

% end mdlTerminate
