global model
% Plot stumbler outcome
figure(1)
set(1, 'units', 'normalized', 'position', [0.1 0.1 0.8 0.8])
h = axes;

% Lengths
Lstance     = model.Lstance;    % [m]
Lhip        = model.Lhip;       % [m]
Lthigh      = model.Lthigh;     % [m]
Lshank      = model.Lshank;     % [m]
Lfoot       = model.Lfoot;      % [m]

% Centres of gravity with respect to proximal joint
cgStance    = model.cgStance;   % [m]
cgThigh     = model.cgThigh;    % [m]
cgShank     = model.cgShank;    % [m]
cgFoot      = model.cgFoot;     % [m]

% % Part B: Brick coordinates
bx1         = model.bx1;
bx2         = model.bx2;
by1         = model.by1;
by2         = model.by2;
bz1         = model.bz1;
bz2         = model.bz2;

for jj = [3, 1]
    for kk = 1:jj:length(X_out.time)
        % Get angles from the correct index of X_out.signals.values
        % Example gamma1 = X_out.signals.values(kk,1);      
        
        gamma1 = X_out.signals.values(kk,1);
        alpha2 = X_out.signals.values(kk,2);
        beta2 = X_out.signals.values(kk,3);
        gamma2 = X_out.signals.values(kk,4);
        gamma3 = X_out.signals.values(kk,5);
        gamma4 = X_out.signals.values(kk,6);

        symb_Ti;
        % Mass locations (Hint; use values from Ti)
        xmass = Ti(4:6:22);
        ymass = Ti(5:6:23);
        zmass = Ti(6:6:24);

        % Joint locations (Hint; use values from Ti)
        xjoint = [0; Ti(1:6:25)];
        yjoint = [0; Ti(2:6:26)];
        zjoint = [0; Ti(3:6:27)];

        subplot(223) % Front view
        plot([0; Ti(3:3:end)], [0; Ti(2:3:end)], 'b-', 'linewidth', 2); hold on
        plot(zjoint, yjoint, 'bo', 'markerfacecolor', 'w');
        plot(zmass, ymass, 'ks', 'markerfacecolor', 'k'); 
        rectangle('Position',[bz1 by1 (bz2-bz1) (by2-by1)],'FaceColor','m')
        plot([-1 1], [0 0], 'k--'); hold off
        axis([-1 1 -0.1 1])
        title('Front view')

        subplot(221) % Top view
        plot([0; Ti(3:3:end)], [0; Ti(1:3:end)], 'b-', 'linewidth', 2); hold on   
        rectangle('Position',[bz1 bx1 (bz2-bz1) (bx2-bx1)],'FaceColor','m')
        plot(zjoint, xjoint, 'bo', 'markerfacecolor', 'w');
        plot(zmass, xmass, 'ks', 'markerfacecolor', 'k'); hold off    
        axis([-1 1 -1 1]);
        title('Top view')

        subplot(224) % Right view
        plot([0; Ti(1:3:end)],  [0; Ti(2:3:end)], 'b-', 'linewidth', 2); hold on
        rectangle('Position',[bx1 by1 (bx2-bx1) (by2-by1)],'FaceColor','m')        
        plot([-1 1], [0 0], 'k--'); hold off
        axis([-1 1 -0.1 1])
        text(-0.9, 0.9, ['TIME: ', num2str(X_out.time(kk)), ' s.'])
        title('Right view')

        subplot(2,2,2) % 3D view
        plot3([0; Ti(3:3:end)], [0; Ti(1:3:end)], [0; Ti(2:3:end)], 'b-', 'linewidth', 2); hold on
        plot3(zjoint, xjoint, yjoint, 'bo', 'markerfacecolor', 'w'); 
        
        %plot brick 
        vert = [bz1 bx1 by1;  ...
        bz1 bx2 by1;  ...
         bz2 bx2 by1;  ...
         bz2 bx1 by1; ...
        bz1 bx1 by2; ...
         bz1 bx2 by2;  ...
         bz2 bx2 by2; ...
          bz2 bx1 by2]; 
        fac = [1 2 3 4; ...
        2 6 7 3; ...
        4 3 7 8; ...
        1 5 8 4; ...
        1 2 6 5; ...
        5 6 7 8];
        patch('Faces',fac,'Vertices',vert,'FaceColor','r');  % patch function

        plot3(zmass, xmass, ymass, 'ks', 'markerfacecolor', 'k'); hold off
        axis([-1 1 -1 1 0 1])
        view([1 0.5 0.5])
        xlabel('z'); ylabel('x'); zlabel('y')
        grid on
        title('3D view')

        drawnow
    end
    pause(0.5)
end
disp('END of animation')