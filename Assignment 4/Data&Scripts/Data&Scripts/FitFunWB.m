function [dum]=FitFun(p,ps,mfr,Hall,Hemgall,Cohall,Cohemgall,U0)

% versie 20 aug 2004

P(1:length(ps),1)=p(1:length(ps));
P(1,1:3)=p(1);  % mass
P(2:5,1:3)=p(2:5)*U0(1:3); % intrinsic scale with measured U0
P(9,1:3)=p(9);  % Td
P([6:8],2)=p(10:12);
P([6:8],3)=p(13:15);
P=P.*(ps*ones(1,3));
clear p

dum=[];
for loop=1:size(P,2)
    p=P(:,loop);
    H=Hall(:,loop);
    Hemg=Hemgall(:,loop);
    Coh=Cohall(:,loop);
    Cohemg=Cohemgall(:,loop);
    
    [Hfx,Hxemg]=ArmModel(p,mfr);
    dum1=sqrt(Coh./(1+mfr)).*abs(log(Hfx./H));
    dum2=sqrt(Cohemg./(1+mfr)).*abs(log((Hxemg+0.0001)./Hemg));
    dum=[dum;dum1;0.3*dum2];
end
