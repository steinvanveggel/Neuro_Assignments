function ParIdentWB(SubjectName)

% ParIdentWB(SubjectName)
%
% parameter identification
%

% versie 20 aug 2004

fin=[1 2 3];

% standard DIR's
RootDir=RootDirectory;
DataDir=[RootDir filesep 'Data' filesep 'RawData' filesep SubjectName filesep];
SpectraDir=[RootDir filesep 'Data' filesep 'Spectra' filesep SubjectName filesep];
TimeDataDir=[RootDir filesep 'Data' filesep 'Time' filesep SubjectName filesep];
ParDir=[RootDir filesep 'Data' filesep 'Parameters' filesep SubjectName filesep];
if exist(ParDir)~=7
    mkdir([RootDir filesep 'Data' filesep 'Parameters' filesep],SubjectName);
end

%settings file inladen
load(strcat(TimeDataDir, 'settings'))

% echte U0
for loop=1:length(trial)
    FileName=trial(loop,1).name;
    load(strcat(SpectraDir, FileName),'avrEMG')
    U0(loop,:)=avrEMG;
end
U0=[mean(U0(fin,:),2) U0(fin,:)];

ABCD={'' 'a' 'b' 'c' 'd'};
for loopabcd=1:5
    %mfrall=[];
    Hall=[];
    Hemgall=[];
    Cohall=[];
    Cohemgall=[];
    for loop1=1:length(fin);%(length(trial))
        FileName=trial(fin(loop1),1).name;
        load(strcat(SpectraDir, FileName, ABCD{loopabcd}))
        mfr=mfr(mn);
        Coh=(abs(mGwx(mn).^2)./mGww(mn)./mGxx(mn));
        Cohemg=(abs(mGwe(mn).^2)./mGww(mn)./mGee(mn));
        % via spectral densities of Arx
            H=-(mGwx(mn)./mGwf(mn));
            Hemg=-(mGwe(mn)./mGwx(mn));
        Hall=[Hall H];
        Hemgall=[Hemgall Hemg];
        Cohall=[Cohall Coh];
        Cohemgall=[Cohemgall Cohemg];
    end

    % P = [m b k bh kh ka kv kp Td].';
    % m, mass
    % b, damping
    % k, stiffness
    % bh, hand(grip) damping
    % kh, hand(grip) stiffness
    % ka, acceleration feedback gain
    % kv, velocity feedback gain
    % kp, position feedback gain
    % (kf, force feedback gain)(Not necessary in this calculation)
    % Td, neural time-delay


    %kp was -600
    pmin=[1 10 10 25 100,...
        0 0 0.1,...
        0.020].';
    % k was 2000
    pmax=[4 100 2000 800 40000,...
        10 200 2000,...
        0.06].';
    ps=[2 10 100 100 20000,...
        ones(1,3),...
        0.025].';
    p0=[2 40 100 50 10000,...
        1 20 400,...
        0.030].';
    % scaling
    pmin=pmin./ps;
    pmax=pmax./ps;
    pstart=p0./ps;

    pminsl=[1 5 10 10 100 -10 -10 -100 0.01].'./ps;
    pmaxsl=[4 100 1000 200 50000 10 100 1000 0.06].'./ps;
    pstartsl=[2 20 200 20 2000 0 0 0 0.06].'./ps;

    %linken
    pmax=[pmax;pmax([6:8]);pmax([6:8])];
    pmin=[pmin;pmin([6:8]);pmin([6:8])];
    pstart=[pstart;pstart([6:8]);pstart([6:8])];

    % zoek random startwaarde
    [dum]=FitFunWB(pstart,ps,mfr,Hall,Hemgall,Cohall,Cohemgall,U0);
    Jp=dum.'*dum;
    for loopPS=1:5000
        ptmp=(pmax-pmin).*rand(length(pstart),1)+pmin;
        [dum]=FitFunWB(ptmp,ps,mfr,Hall,Hemgall,Cohall,Cohemgall,U0);
        Jtmp=dum.'*dum;
        if Jtmp<Jp
            Jp=Jtmp;pstart=ptmp;
            disp(['loopPS: ' num2str(loopPS) '  ' num2str(Jp)])
        end
    end
    %optimize
    options=optimset('Display','iter','MaxIter',1000,'TolFun',1e-5,'DiffMinChange',1e-2);
    pstartb=lsqnonlin('FitFunWB',pstart,pmin,pmax,options,ps,mfr,Hall,Hemgall,Cohall,Cohemgall,U0);
    options=optimset('Display','iter','MaxFunEvals',1e6,'MaxIter',1e5,'TolFun',1e-6,'DiffMinChange',1e-3); % was 7 4
    [p,Jp,e,dum2,dum3,dum4,J]=lsqnonlin('FitFunWB',pstartb,pmin,pmax,options,ps,mfr,Hall,Hemgall,Cohall,Cohemgall,U0);
    stxx=diag(inv(J'*J)/length(e)*sum(e.^2)/length(e))/length(e);
    stxx=full(stxx);

    %values
    P(1:length(ps),1)=p(1:length(ps));
    P(1,1:3)=p(1);  % mass
    P(2:5,1:3)=p(2:5)*U0(1:3); % intrinsic scale with measured U0
    P(9,1:3)=p(9);  % Td
    P([6:8],2)=p(10:12);
    P([6:8],3)=p(13:15);
    P=P.*(ps*ones(1,3))
  

    J=FitFunWB_sep(P,ps,mfr,Hall,Hemgall,Cohall,Cohemgall,U0)

    %
    SaveFile=['ParWB' ABCD{loopabcd}];

    save(strcat(ParDir, SaveFile), 'P', 'p', 'stxx', 'Jp', 'J')
end