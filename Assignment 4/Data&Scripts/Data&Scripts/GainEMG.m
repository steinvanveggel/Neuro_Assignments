function GainEMG(SubjectName,Check)

% GainEMG(SubjectName,Check)
%
% Calculates EMG-to-torque ratio's
%
% SubjectName,  Name of the subject
% Check,        optioneel, indien 1 dan ook plotjes

% versie 27 juli 2004

if nargin==1
    Check=0;
end

% standard DIR's
RootDir=RootDirectory;
DataDir=[RootDir filesep 'Data' filesep 'RawData' filesep SubjectName filesep];
SpectraDir=[RootDir filesep 'Data' filesep 'Spectra' filesep SubjectName filesep];

disp(['EMG to force ratio: ' SubjectName])

% load EMGnorm (G,A1,A2,A3,A4)
load(strcat(SpectraDir, '\EMGnorm'))
cd(DataDir)

% during rest
n=1;
% EMGs tijdens rust; 2 seconden voor en na wegknippen
load('before4')
e1b=filter(A1,1,e1)*G(1);
e2b=filter(A2,1,e2)*G(2);
e3b=filter(A3,1,e3)*G(3);
e4b=filter(A4,1,e4)*G(4);
fcb=fc;
load('after4')
e1a=filter(A1,1,e1)*G(1);
e2a=filter(A2,1,e2)*G(2);
e3a=filter(A3,1,e3)*G(3);
e4a=filter(A4,1,e4)*G(4);
fca=fc;
nstart=5001;
mfcb(n)=0;%mean(fcb(nstart:nstart+26*2500));
me1b(n)=mean(abs(e1b(nstart:nstart+26*2500)));
me2b(n)=mean(abs(e2b(nstart:nstart+26*2500)));
me3b(n)=mean(abs(e3b(nstart:nstart+26*2500)));
me4b(n)=mean(abs(e4b(nstart:nstart+26*2500)));
mfca(n)=0;%mean(fca(nstart:nstart+26*2500));
me1a(n)=mean(abs(e1a(nstart:nstart+26*2500)));
me2a(n)=mean(abs(e2a(nstart:nstart+26*2500)));
me3a(n)=mean(abs(e3a(nstart:nstart+26*2500)));
me4a(n)=mean(abs(e4a(nstart:nstart+26*2500)));

% EMGs tijdens taken
for loop=1:3
    load(strcat(DataDir, 'before', num2str(loop)))
    e1b=filter(A1,1,e1)*G(1);
    e2b=filter(A2,1,e2)*G(2);
    e3b=filter(A3,1,e3)*G(3);
    e4b=filter(A4,1,e4)*G(4);
    fcb=fc;
    load(strcat(DataDir, 'after', num2str(loop)))
    e1a=filter(A1,1,e1)*G(1);
    e2a=filter(A2,1,e2)*G(2);
    e3a=filter(A3,1,e3)*G(3);
    e4a=filter(A4,1,e4)*G(4);
    fca=fc;
    %inlezen kracht en gemiddelde emg per blok
    % 3 blokken van elk 10s,op 2500 Hz
    % eerste 2 seconden  en laatste 2 seconden weg laten!
    for loop2=1:3
        nstart=(loop2-1)*25000+5001;
        if n<9
            n=n+1;
            mfcb(n)=mean(fcb(nstart:nstart+6*2500));
            me1b(n)=mean(abs(e1b(nstart:nstart+6*2500)));
            me2b(n)=mean(abs(e2b(nstart:nstart+6*2500)));
            me3b(n)=mean(abs(e3b(nstart:nstart+6*2500)));
            me4b(n)=mean(abs(e4b(nstart:nstart+6*2500)));
            mfca(n)=mean(fca(nstart:nstart+6*2500));
            me1a(n)=mean(abs(e1a(nstart:nstart+6*2500)));
            me2a(n)=mean(abs(e2a(nstart:nstart+6*2500)));
            me3a(n)=mean(abs(e3a(nstart:nstart+6*2500)));
            me4a(n)=mean(abs(e4a(nstart:nstart+6*2500)));
        end
    end
    clear w x fc e1 e2 e3 e4 e1a e2a e3a e4a e1b e2b e3b e4b
end

cd(RootDir)

% sorteren
[mfca,nsort]=sort(mfca);
me1a=me1a(nsort);
me2a=me2a(nsort);
me3a=me3a(nsort);
me4a=me4a(nsort);
[mfcb,nsort]=sort(mfcb);
me1b=me1b(nsort);
me2b=me2b(nsort);
me3b=me3b(nsort);
me4b=me4b(nsort);

% spier 1&2 zijn 'duwwers'; spier 3&4 zaijn trekkers
% positieve kracht = duwwen
fduw=[mfcb(5:9) mfca(5:9)];
e1=[me1b(5:9) me1a(5:9)];
e2=[me2b(5:9) me2a(5:9)];
ftrek=[mfcb(1:5) mfca(1:5)];
e3=[me3b(1:5) me3a(1:5)];
e4=[me4b(1:5) me4a(1:5)];

% schatten
% emg ingang, torque uitgang!
% emg ingang, kracht uitgang!
options=optimset('Display','off');
q0=[1;0];
qe1=lsqcurvefit('axplusb',q0,e1,fduw,[],[],options);
qe2=lsqcurvefit('axplusb',q0,e2,fduw,[],[],options);
qe3=lsqcurvefit('axplusb',q0,e3,ftrek,[],[],options);
qe4=lsqcurvefit('axplusb',q0,e4,ftrek,[],[],options);

% for development: plotten
if Check==1
    figure, % nog aanpassen aan 4 spieren!
    subplot(221), plot(mfcb,me1b,'bx'), hold on, plot(mfca,me1a,'rx')
    ylabel(SubjectName),title('flexor'),xlabel('Torque [Nm]')
    subplot(222), plot(mfcb,me2b,'bx'), hold on, plot(mfca,me2a,'rx')
    title('extensor'),xlabel('Torque [Nm]')
    subplot(221), plot(axplusb(qe1,e1),e1,'mo')
    legend({'before','after','fit'})
    subplot(222), plot(axplusb(qe2,e2),e2,'mo')
    
    subplot(223), plot(mfcb,me3b,'bx'), hold on, plot(mfca,me3a,'rx')
    ylabel(SubjectName),title('flexor'),xlabel('Torque [Nm]')
    subplot(224), plot(mfcb,me4b,'bx'), hold on, plot(mfca,me4a,'rx')
    title('extensor'),xlabel('Torque [Nm]')
    subplot(223), plot(axplusb(qe3,e3),e3,'mo')
    legend({'before','after','fit'})
    subplot(224), plot(axplusb(qe4,e4),e4,'mo')
end

% gains saven (kemg)
Q=[qe1(1);qe2(1);qe3(1);qe4(1)];
kemg=Q/2;
cd(SpectraDir)
save('EMGgain','kemg');



% for development: plotten
if Check==1
    % tijd vector, nog aanpassen aan 4 spieren!
    t=(0:75000)/2500;
    % activatiedynamica 2.3 Hz
    [b,a]=butter(3,2.3/1250);
    figure
    cd(DataDir)
    load('before1')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(411), plot(t,fc), hold on
    %axis([0 10 -3 3])
    title(SubjectName)
    ylabel('before 1')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'m')
    load('before2')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(412), plot(t,fc), hold on
    %axis([0 10 -3 3])
    ylabel('before 2')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'m')
    load('before3')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(413), plot(t,fc), hold on
    %axis([0 10 -3 3])
    ylabel('before 3')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'m')
    load('before4')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(414), plot(t,fc), hold on
    %axis([0 10 -3 3])
    ylabel('before 4')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'m')
    xlabel('time [s]')
    legend({'recorded torque','estimated torque from EMG'})
        
    figure
    load('after1')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(411), plot(t,fc,'k'), hold on
    %axis([0 10 -3 3])
    title(SubjectName)
    ylabel('after 1')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'r')
    load('after2')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(412), plot(t,fc,'k'), hold on
    %axis([0 10 -3 3])
    ylabel('after 2')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'r')
    load('after3')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(413), plot(t,fc,'k'), hold on
    %axis([0 10 -3 3])
    ylabel('after 3')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'r')
    load('after4')
%     fc=matrix(3,:).';
%     e1=matrix(9,:).';
%     e2=matrix(10,:).';
    subplot(414), plot(t,fc,'k'), hold on
    %axis([0 10 -3 3])
    ylabel('after 4')
    e1=filter(A1,1,e1)*G(1);
    e2=filter(A2,1,e2)*G(2);
    e3=filter(A3,1,e3)*G(3);
    e4=filter(A4,1,e4)*G(4);
    e=(abs(e1)*kemg(1)+abs(e2)*kemg(2)+abs(e3)*kemg(3)+abs(e4)*kemg(4));
    ef=filter(b,a,e);
    plot(t,ef,'r')
    xlabel('time [s]')
    legend({'recorded torque','estimated torque from EMG'})
end

cd(RootDir)