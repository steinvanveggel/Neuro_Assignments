function WhiteEMG(SubjectName)

% WhiteEMG(SubjectName)
%
% 1) whitening filter EMG
% 2) normalize EMG with respect to broadband disturbance
%
% SubjectName, Name of the subject

% versie 24 augustus 2004

% NrRep
NrRep=4;
FileName='bk0';

% standard DIR's
RootDir=RootDirectory;
DataDir=[RootDir filesep 'Data' filesep 'RawData' filesep SubjectName filesep];
SpectraDir=[RootDir filesep 'Data' filesep 'Spectra' filesep SubjectName filesep];
if exist(SpectraDir)~=7
    mkdir([RootDir filesep 'Data' filesep 'Spectra' filesep],SubjectName);
end

disp(['Prewhitening: ' SubjectName])

% EMGs tijdens taken
% haal whitening filter uit de duw en trek taken vooraf!
load(strcat(DataDir, 'before1'))
nstart=5000; % na 2 s
e1=e1(nstart+1:nstart+6*2500);
e2=e2(nstart+1:nstart+6*2500);
load(strcat(DataDir, 'before2'))
nstart=55000; % na 22 s
e3=e3(nstart+1:nstart+6*2500);
e4=e4(nstart+1:nstart+6*2500);

% Calculate spectral densities and whitening filter (A1,A2)
N=length(e4);
Gee1=abs(fft(e1).^2);Gee1=Gee1(1:N/2+1);
Gee2=abs(fft(e2).^2);Gee2=Gee2(1:N/2+1);
Gee3=abs(fft(e3).^2);Gee3=Gee3(1:N/2+1);
Gee4=abs(fft(e4).^2);Gee4=Gee4(1:N/2+1);
f=(0:N/2)/N*2500;
No=6; % 6ste orde
[Pxx,F,A1]=pmem(e1,No,N,2500);
[Pxx,F,A2]=pmem(e2,No,N,2500);
[Pxx,F,A3]=pmem(e3,No,N,2500);
[Pxx,F,A4]=pmem(e4,No,N,2500);

% bepalen EMGNorm (G)
ABCD={'a' 'b' 'c' 'd'};
for loop2=1:NrRep
    load(strcat(DataDir,FileName,ABCD{loop2}))
    xall(:,:,loop2)=x;
    wall(:,:,loop2)=w;
    fcall(:,:,loop2)=fc;
    emgall(:,:,loop2)=emg;
end
% standard definitions
Fs = 2500;
N = 2^16;
Ntot=30*Fs+1;
Ncut=1000;
%
emgwall=[filter(A1,1,emgall(:,1,:)) filter(A2,1,emgall(:,2,:)) filter(A3,1,emgall(:,3,:)) filter(A4,1,emgall(:,4,:))] ;
%
G=1./mean(mean(abs(emgwall(Ntot-N+1-Ncut:Ntot-Ncut,:,:)),1),3).';

% save results
cd(SpectraDir)
save('EMGnorm','G','A1','A2', 'A3', 'A4')
cd(RootDir)
