function [Hfx,Hxemg]=ArmModel(p,mfr);

% [Hfx,Hxemg]=ArmModel(p,mfr);
%
% 18 maart 2003

% parameters
Int=p(1);
CE=p(2:3);
Hand=p(4:5);
if length(p)==5
    MS=[0 0 0 0].';
    GTO=0;
    MN=0;
    %ACT=0.030;
    Td=0.025;
    Td2=0.025;
elseif length(p)==9
    MS=[p(6:8);0];
    GTO=0;
    MN=0;
    %ACT=0.030;
    Td=p(9);
    Td2=p(9);
elseif length(p)==10
    MS=[p(6:7);0;p(8)];
    GTO=0;
    MN=0;
    %ACT=0.030;
    Td=p(9);
    Td2=p(10);
elseif length(p)==11
    MS=[p(6:8);p(10)];
    GTO=0;
    MN=0;
    %ACT=0.030;
    Td=p(9);
    Td2=p(11);
end

% model
s=2*pi*mfr*1i;
s2=(-4*pi^2)*mfr.^2;
%
Cint=Int(1)*s2;
Hms=(MS(1)*s2+MS(2)*s+MS(3)).*exp(-Td*s)+MS(4).*exp(-Td2*s);
Cce=CE(1)*s+CE(2);
%Hact=1./(ACT*s+1);
w0=2.18*2*pi;
beta=0.75;
a1=1/w0^2;a2=2*beta/w0;
Hact=1./(a1*s2+a2*s+1);
Hgto=GTO(1).*exp(-Td*s);
Hfilt=1./(1+Hgto.*Hact);
Ch=Hand(1)*s+Hand(2);
% overdracht arm
Hm=1./( Cint+(Cce+Hms.*Hact).*Hfilt );
% overdracht x-emg
Hxa=(Hms-Hgto.*Cce).*Hfilt;
% invloed hand
Hxa=Hxa.*( (Hm.*Ch)./(1+Ch.*Hm) );
Hfx=Hm+(1./Ch);
Hxemg=Hxa;
