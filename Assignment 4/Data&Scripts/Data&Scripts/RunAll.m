subjectname = 'sub1';
cd('C:\Users\annek\Documents\Studie\Git\Repos\Neuro_Assignments\Assignment 4\Data&Scripts\Data&Scripts')

WhiteEMG(subjectname); % conditions the EMG
GainEMG(subjectname); % calculate EMG-force relation
SaveLocal(subjectname); % convert data and resample data
NonParIdent(subjectname); % convert to frequency domain (Bode!)
ParIdentWB(subjectname) % fit model onto the Bode diagrams
PlotSpectra(subjectname,'be')% plot the Bode, (evt. with model fit)

% Question 5
addpath('C:\Users\annek\Documents\Studie\Git\Repos\Neuro_Assignments\Assignment 4\Data&Scripts\Data&Scripts\Data\Parameters\sub1')
addpath('C:\Users\annek\Documents\Studie\Git\Repos\Neuro_Assignments\Assignment 4\Data&Scripts\Data&Scripts\Data\Parameters\sub1')
load('ParWB.mat')

names = {'Arm mass (kg)','Muscle Viscosity (Ns/m)','Muscle elasticity (N/m)','Grip viscosity (Ns/m)','Grip stiffness (N/m)','Acceleration feedback gain','Velocity feedback gain','Position feedback gain','Neural time delay (s)'};
figure
for i = 1:9
    nexttile
    bar(P(i,:))
    title(names(i))
end

% Question 6
i=3;
p.m=P(1,i);     % mass [kg]
p.b=P(2,i);     % muscle viscosity [Ns/m]
p.k=P(3,i);     % muscle elasticty
p.bc=P(4,i);    % grip viscosity [Ns/m]
p.kc=P(5,i);    % grip stiffness[N/m]
p.ka=P(6,i);    % acceleration feedback gain
p.kv=P(7,i);    % velocity feedback gain 
p.kp=P(8,i);    % position feedback gain
p.Td=P(9,i);    % neural time delay [s]

% f: frequency vector
f=(0.1:0.1:20); % [Hz]

% call the script
% H: structure with FRFs
%   e.g. H.Hfx is the FRF of the human joint
%        H.Hdx is the FRF of the combined system (joint with environment)
[H,p,J]=NMC_tf(p,f);

% plot the result in Bode diagrams
%  left: the mechanical admittance of the joint and the combined system
%  right: the reflexive impedance 
figure
subplot(221)
loglog(H.f,abs(H.Hfx)), hold on
loglog(H.f,abs(H.Hdx),'r--')
xlim([min(H.f) max(H.f)])
title('Mechanical Admittance (H_{fx})')
ylabel('gain [m/N]')
subplot(223)
semilogx(H.f,unwrap(angle(H.Hfx))*180/pi), hold on
semilogx(H.f,unwrap(angle(H.Hdx))*180/pi,'r--')
xlim([min(H.f) max(H.f)])
set(gca,'YTick',-360:90:360)
ylim([-180,0])
legend({'joint','total'})
ylabel('phase [\circ]')
xlabel('frequency [Hz]')

subplot(222)
loglog(H.f,abs(H.Hxa))
xlim([min(H.f) max(H.f)])
title('Reflexive Impedance (H_{xa})')
ylabel('gain [N/m]')
subplot(224)
semilogx(H.f,unwrap(angle(H.Hxa))*180/pi)
xlim([min(H.f) max(H.f)])
set(gca,'YTick',-360:90:360)
ylim([-180,180])
ylabel('phase [\circ]')
xlabel('frequency [Hz]')