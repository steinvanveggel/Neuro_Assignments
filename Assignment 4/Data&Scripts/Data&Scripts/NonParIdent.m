function NonParIdent(SubjectName,Group)

% NonParIdent(SubjectName,Group)
%
% SubjectName, Name of the subject

% versie 24 augustus 2004

if nargin==1
    Group='Nor';
end
% NrRep
if strncmp(Group,'Nor',3)==1
    NrRep=4;
elseif  strncmp(Group,'CVA',3)==1
    NrRep=4;
elseif  strncmp(Group,'PD',2)==1
    NrRep=4;
elseif strncmp(Group,'RSI',3)==1
    NrRep=2;
elseif strncmp(Group,'Neg',3)==1
    NrRep=4;
end

% standard DIR's
RootDir=RootDirectory;
DataDir=[RootDir filesep 'Data' filesep 'RawData' filesep SubjectName filesep];
SpectraDir=[RootDir filesep 'Data' filesep 'Spectra' filesep SubjectName filesep];
TimeDataDir=[RootDir filesep 'Data' filesep 'Time' filesep SubjectName filesep];

%settings file inladen
load(strcat(TimeDataDir, 'settings'))

disp(['Converting to frequency domain: ' SubjectName])
% inlees loopje
ABCD={'' 'a' 'b' 'c' 'd'};
for loopabcd=1:(NrRep+1)
    for loop1=1:length(trial)
        FileName=[trial(loop1,1).name ABCD{loopabcd}];
        %disp(['Inlezen ' FileName])
        load(strcat(TimeDataDir, FileName))
        [mfr,mGww,mGxx,mGff,mGee,mGwx,mGwf,mGwe,mn]=CalcSpectra(wr,xr,fcr,emgr);
        save(strcat(SpectraDir,FileName), 'mfr','mGww', 'mGxx', 'mGff', 'mGee', 'mGwx', 'mGwf', 'mGwe', 'mn', 'avrEMG', 'stdWX');
    end
end
