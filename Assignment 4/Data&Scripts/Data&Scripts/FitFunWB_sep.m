function [J]=FitFun(p,ps,mfr,Hall,Hemgall,Cohall,Cohemgall,U0)

P=p;

for loop=1:size(P,2)
    p=P(:,loop);
    H=Hall(:,loop);
    Hemg=Hemgall(:,loop);
    Coh=Cohall(:,loop);
    Cohemg=Cohemgall(:,loop);
    
    [Hfx,Hxemg]=ArmModel(p,mfr);
    
    %     dum1=sqrt(Coh).*abs(log(Hfx./H));
    %     dum2=sqrt(Cohemg).*abs(log(Hxemg./Hemg));
    %     J(1,loop)=sum(dum1.^2);
    %     J(2,loop)=sum((0.3*dum2).^2);
    
    dum1=sqrt(Coh./(1+mfr)).*abs(log(Hfx./H));
    dum2=sqrt(Cohemg./(1+mfr)).*abs(log(Hxemg./Hemg));
    J(1,loop)=sum(dum1.^2);
    J(2,loop)=sum((0.3*dum2).^2);
%     figure(loop);
%     subplot(321), loglog(mfr,abs(Hall(:,loop)),'b',mfr,abs(Hfx),'r'), hold on
%     subplot(323), semilogx(mfr,angle(Hall(:,loop))*180/pi,'b',mfr,angle(Hfx)*180/pi,'r'), hold on
%     subplot(322), loglog(mfr,abs(Hemgall(:,loop)),'b',mfr,abs(Hxemg),'r'), hold on
%     subplot(324), semilogx(mfr,angle(Hemgall(:,loop))*180/pi,'b',mfr,angle(Hxemg)*180/pi,'r'), hold on
end
