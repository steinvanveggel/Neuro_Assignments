function PlotSpectra(SubjectName,grp,Fit)

% PlotSpectra(SubjectName,Method,grp,Fit)
%
%
% SubjectName, Name of the subject
% grp, the group [be, ke, or kn], i.e. damping, stiffness, negative stiffness
% Fit [optional], optioneel, indien 1 dan ook de fits

if nargin==2
    Fit=0;
end

% NrRep
if strncmpi(grp,'be',2)
    FileNames={'bk0';'b100';'b200'};
    lgdtxt={'reference','100 Ns/m','200 Ns/m'};
elseif strncmpi(grp,'ke',2)
    FileNames={'bk0';'k300';'k600'};
    lgdtxt={'reference','300 N/m','600 N/m'};
elseif strncmpi(grp,'kn',2)
    FileNames={'bk0';'kn300';'kn600'};
    lgdtxt={'reference','-300 N/m','-600 N/m'};
else
    error('wrong grp')
end
lc=[0.6 0.6 0.6;0 0 1;1 0 0]; %color
lw=[4;ones(2,1)];                           %width
lm={'.';'.';'.'};         %marker

    
    
% standard DIR's
RootDir=RootDirectory;
DataDir=[RootDir filesep 'Data' filesep 'RawData' filesep SubjectName filesep];
SpectraDir=[RootDir filesep 'Data' filesep 'Spectra' filesep SubjectName filesep];
TimeDataDir=[RootDir filesep 'Data' filesep 'Time' filesep SubjectName filesep];
ParDir=[RootDir filesep 'Data' filesep 'Parameters' filesep SubjectName filesep];

%settings file inladen
load(strcat(TimeDataDir, 'settings'))

% make figure
h1=figure;
for loop1=1:length(FileNames)
    FileName=FileNames{loop1};
    load(strcat(SpectraDir, FileName))
    % FRF and coherences
    Coh=abs(mGwx(mn).^2)./mGww(mn)./mGxx(mn);
    Cohemg=abs(mGwe(mn).^2)./mGww(mn)./mGee(mn);
        H=-mGwx(mn)./mGwf(mn);
        Hemg=-mGwe(mn)./mGwx(mn);
        % the figure
        figure(h1)
        subplot(321), loglog(mfr(mn),abs(H),'Color',lc(loop1,:),'LineWidth',lw(loop1),'Marker',lm{loop1}), hold on
        subplot(323), semilogx(mfr(mn),unwrap(angle(H))*180/pi,'Color',lc(loop1,:),'LineWidth',lw(loop1),'Marker',lm{loop1}), hold on
        subplot(322), loglog(mfr(mn),abs(Hemg),'Color',lc(loop1,:),'LineWidth',lw(loop1),'Marker',lm{loop1}), hold on
        subplot(324), semilogx(mfr(mn),unwrap(angle(Hemg))*180/pi,'Color',lc(loop1,:),'LineWidth',lw(loop1),'Marker',lm{loop1}), hold on
    subplot(325), semilogx(mfr(mn),Coh,'Color',lc(loop1,:),'LineWidth',lw(loop1),'Marker',lm{loop1}), hold on
    subplot(326), semilogx(mfr(mn),Cohemg,'Color',lc(loop1,:),'LineWidth',lw(loop1),'Marker',lm{loop1}), hold on
end

% figure titles etc
figure(h1)
subplot(321), axis([0.5 20 1e-5 1e-2]), ylabel('Gain [m/N]','FontSize',13), title(['',' F => X'],'FontSize',16), set(gca,'YTick',[1e-5 1e-4 1e-3 1e-2 1e-1 1 1e1]);
subplot(323), axis([0.5 20 -180 90]), ylabel('Phase [\circ]','FontSize',13),set(gca,'YTick',[-270:90:270])
subplot(325), axis([0.5 20 0 1]), ylabel('{\gamma_x}^2 [-]','FontSize',13), xlabel('frequency [Hz]','FontSize',13)
if isempty(lgdtxt)~=1
    legend(lgdtxt)
end
subplot(322), axis([0.5 20 1e2 1e5]), ylabel('Gain [N/m]','FontSize',13), title('X => EMG','FontSize',16)
subplot(324), axis([0.5 20 -180 180]), ylabel('Phase [\circ]','FontSize',13),set(gca,'YTick',[-360:90:360])
subplot(326), axis([0.5 20 0 1]), ylabel('{\gamma_a}^2 [-]','FontSize',13), xlabel('frequency [Hz]','FontSize',13)

% plot de fits
if Fit==1
    load(strcat(ParDir, 'ParWB'))
    mfr=0.5:0.1:20;
    for loop1=1:length(FileNames)
        p=P(:,loop1);
        % plotten
        [Hfx,Hxemg]=ArmModel(p,mfr);
        
        figure(h1)
        subplot(321), loglog(mfr,abs(Hfx),'Color',lc(loop1,:),'LineStyle','--')
        subplot(323), semilogx(mfr,unwrap(angle(Hfx))*180/pi,'Color',lc(loop1,:),'LineStyle','--')
        subplot(322), loglog(mfr,abs(Hxemg),'Color',lc(loop1,:),'LineStyle','--')
        subplot(324), semilogx(mfr,unwrap(angle(Hxemg))*180/pi,'Color',lc(loop1,:),'LineStyle','--')
    end
end

% A4 format
orient tall

