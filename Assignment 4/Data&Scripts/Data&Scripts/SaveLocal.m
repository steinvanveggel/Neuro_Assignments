function SaveLocal(SubjectName)

% SaveLocal(SubjectName)
%
% average data, convert EMG and save the data
% the data in folder 'RawData' <can> be removed (make backup to CD first!!!)
%
% SubjectName, Name of the subject

% versie 24 aug 2004

    NrRep=4;

% standard DIR's
RootDir=RootDirectory;
DataDir=[RootDir filesep 'Data' filesep 'RawData' filesep SubjectName filesep];
SpectraDir=[RootDir filesep 'Data' filesep 'Spectra' filesep SubjectName filesep];
TimeDataDir=[RootDir filesep 'Data' filesep 'Time' filesep SubjectName filesep];
if exist(TimeDataDir)~=7
    mkdir([RootDir filesep 'Data' filesep 'Time' filesep],SubjectName);
end

%settings file inladen en saven
load(strcat(DataDir, 'settings'))
save(strcat(TimeDataDir,'\settings'), 'trial', 'TrialOrder','setpoint')

% EMG laden (G,A1,A2,A3,A4,kemg)
load(strcat(SpectraDir, 'EMGnorm'))
load(strcat(SpectraDir, 'EMGgain'))

% standaard waarden
Fs = 2500;
N = 2^16;
T = N/2500;
Ntot=30*2500+1;
df=1/T;
f=(0:N-1).'*df;
Ncut=1000;
% resample factor 32; 2500 => 78.125
rs=32;

cd(DataDir)
disp(['Saving local: ' SubjectName])
% inlees loopje
ABCD={'a' 'b' 'c' 'd'};
for loop1=1:length(trial)
    FileName=trial(loop1,1).name;
    for loop2=1:NrRep
        load(strcat(FileName, ABCD{loop2}))
        % prewhitening EMG
        emg=[filter(A1,1,emg(:,1)) filter(A2,1,emg(:,2)) filter(A3,1,emg(:,3)) filter(A4,1,emg(:,4))];
        % calc avrEMG
        avrEMG=mean(abs(emg(Ntot-N+1-Ncut:Ntot-Ncut,:)),1)*G/4;
        % relate emg to force
        emg=abs(emg)*(G.*kemg);
        % calculate RMS values
        stdWX=[ std(w(Ntot-N+1-Ncut:Ntot-Ncut));...
            std(x(Ntot-N+1-Ncut:Ntot-Ncut))];
        % needed for averaging
        wall(:,:,loop2)=w;
        fcall(:,:,loop2)=fc;
        xall(:,:,loop2)=x;
        emgall(:,:,loop2)=emg;
        avrEMGall(1,loop2)=avrEMG;
        % resampling
        w=w-mean(w);
        x=x-mean(x);
        fc=fc-mean(fc);
        emg=emg-mean(emg);
        wr=resample(w,1,rs);
        xr=resample(x,1,rs);
        fcr=resample(fc,1,rs);
        emgr=resample(emg,1,rs);
        % opslaan per herhaling
        save(strcat(TimeDataDir, FileName, ABCD{loop2}),'wr', 'xr', 'fcr', 'emgr', 'avrEMG', 'stdWX');
    end
    % average!
    x=mean(xall,3);
    w=mean(wall,3);
    fc=mean(fcall,3);
    emg=mean(emgall,3);
    avrEMG=avrEMGall;
    % calculate RMS values
    stdWX=[ std(w(Ntot-N+1-Ncut:Ntot-Ncut));...
        std(x(Ntot-N+1-Ncut:Ntot-Ncut))];
    % resampling
    w=w-mean(w);
    x=x-mean(x);
    fc=fc-mean(fc);
    emg=emg-mean(emg);
    wr=resample(w,1,rs);
    xr=resample(x,1,rs);
    fcr=resample(fc,1,rs);
    emgr=resample(emg,1,rs);
    % save each condition, averaged over NrRep!
    save(strcat(TimeDataDir, FileName),'wr', 'xr', 'fcr', 'emgr', 'avrEMG', 'stdWX');
    clear x fc w emg avrEMG stdWX
end

cd(RootDir)