function [mfr,mGww,mGxx,mGff,mGee,mGwx,mGwf,mGwe,mn]=calcspectra(w,x,fc,emg,z)

% Calcspectra
%
% calculates spectral densities

% versie 24 aug 2004

% standaard waarden
Fs = 2500;
N = 2^16;
T = N/2500;
Ntot=30*2500+1;
df=1/T;
f=(1:N/2).'*df;
Ncut=1000;
% verandering door resamplen
rs=32;
Ntotrs=ceil(Ntot/rs);
Ncutrs=round(Ncut/rs);
Nrs=round(N/rs);
dt=rs/Fs;

% knippen en FFT
w=w(Ntotrs-Nrs+1-Ncutrs:Ntotrs-Ncutrs);
x=x(Ntotrs-Nrs+1-Ncutrs:Ntotrs-Ncutrs);
fc=fc(Ntotrs-Nrs+1-Ncutrs:Ntotrs-Ncutrs);
emg=emg(Ntotrs-Nrs+1-Ncutrs:Ntotrs-Ncutrs);

% fft-en
W=fft(w,Nrs);  W=W(2:Nrs/2+1);
X=fft(x,Nrs);  X=X(2:Nrs/2+1);
F=fft(fc,Nrs); F=F(2:Nrs/2+1);
E=fft(emg,Nrs);E=E(2:Nrs/2+1);

%Spectral density estimates
Gww=abs(W).^2;
Gxx=abs(X).^2;
Gff=abs(F).^2;
Gee=abs(E).^2;
Gwx=conj(W).*X;
Gwf=conj(W).*F;
Gwe=conj(W).*E;
%Gxe=conj(X).*E;

%Frequency averaging
m=4;
Nmod=Nrs/2/m;			% aantal frequentie punten na middeling, halve vector!
tem=zeros(m,Nmod);
tem(:)=f(1:Nrs/2); mfr=mean(tem,1).';

%Frequency averaging
tem=zeros(m,Nmod);
tem(:)=Gww;mGww=mean(tem,1).';
tem(:)=Gxx;mGxx=mean(tem,1).';
tem(:)=Gff;mGff=mean(tem,1).';
tem(:)=Gee;mGee=mean(tem,1).';
tem(:)=Gwx;mGwx=mean(tem,1).';
tem(:)=Gwf;mGwf=mean(tem,1).';
tem(:)=Gwe;mGwe=mean(tem,1).';

%afhakken tot 512 (128) punten = 78 Hz (om Mb te besparen!)
% % nlast=512;
% % f=f(1:nlast);
% % Gww=Gww(1:nlast);
% % Gxx=Gxx(1:nlast);
% % Gff=Gff(1:nlast);
% % Gee=Gee(1:nlast);
% % Gwx=Gwx(1:nlast);
% % Gwf=Gwf(1:nlast);
% % Gwe=Gwe(1:nlast);
% % Gwz=Gwz(1:nlast);
% nlast=128;
% mfr=mfr(1:nlast);
% mGww=mGww(1:nlast);
% mGxx=mGxx(1:nlast);
% mGff=mGff(1:nlast);
% mGee=mGee(1:nlast);
% mGwx=mGwx(1:nlast);
% mGwf=mGwf(1:nlast);
% mGwe=mGwe(1:nlast);
% mGwz=mGwz(1:nlast);

% Pieken zoeken
gem=mean(Gww);
n=find(Gww>(gem/10));
gem=mean(mGww);
mn=find(mGww>(gem/10));
