function y=axplusb(p,x)

% y=axplusb(p,x)
% 
% first-order curve fit
% y=p(1)*x+p(2);

y=p(1)*x+p(2);
