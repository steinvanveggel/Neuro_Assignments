function [H,p,J]=NMC_tf(p,f,fxx)
%
% [H] = NMC_tf(p,f)
% [H,p,J] = NMC_tf(p,f)
%
% NMC_tf calculates the frequency response of a neuromusculoskeletal model.
% p is a structure with the parameters, f the frequency vector [Hz], H is a
% structure containing the frequency response functions (FRF), and J is
% structure with the performance measures.
%
% The model includes o.a. afferent feedback (proprioceptive reflexes) for
% position, velocity, acceleration (muscle spindles) and force (Golgi
% Tendon Organs, GTO).
%
% The parameters are fields of p (p.m, p.b, etc):
% - skeletal model
%       mass, passive visco-elasticity          m, bq, kq
% - muscle model
%       lumped visco-elasticity (co-contracion) b, k
%       series elastic element [optional]       kse
%       activation dynamics                     F0, Bt
% - afferent feedback model
%       feedback gains                          kp, kv, ka, kf
%       neural time delay                       Td
% - contact dynamics [optional]
%       contact visco-elasticity                bc, kc
% - environment model [optional]
%       mass, damping & stiffness               me, be, ke
%
% FRF structure
% H.f     frequency vector (same as f)
% H.Hfx   mechanical admittance of the human joint
% H.Hxa   reflexive impedance
% H.Hdx   total admittance (joint & environment)
% H.Holh  open loop gain, human joint only
% H.Holt  open loop gain, total system (joint, grip and environment)
% H.unsh  unstable (if nonzero), human joint only
% H.unst  unstable (if nonzero), total system
%
%
% The performance, J, is normalized and defined as:
%    J=var(x)=|Hfx|^2 (see Schouten et al. 2001, Biol Cyber 84;143-152.)
%
% J.xh    displacements of the human joint (assuming no environment)
% J.xt    displacements of the combined system (joint with environment)
%
% The model is described in:
% "NMClab, a model to assess the contributions of muscle visco-elasticity
% and afferent feedback to joint dynamics."
% Schouten, Mugge, van der Helm. J Biomech, 2008, accepted.
%
% version 0.91: pre-release, February 29, 2008
% Copyright (C) 2007-2008, Alfred C. Schouten
% This software and model may be used, copied, or redistributed as long as
% it is not sold and this copyright notice is reproduced on each copy made.
% This model is provided as is, without any express or implied warranties
% whatsoever. 
%
% Please acknowledge the use of this model and software in any publications
% by refering to the article: 
%  "NMClab, a model to assess the contributions of muscle visco-elasticity
%  and afferent feedback to joint dynamics."
%  Schouten, Mugge, van der Helm. J Biomech, 2008, accepted.
%
% Alfred C. Schouten
% Delft Laboratory for NeuroMuscular Control
% Department of (Bio)Mechanical Engineering
% Delft University of Technology
%
% Mekelweg 2
% 2628 CD Delft
% The Netherlands
% e-mail: A.C.Schouten@tudelft.nl
% url:    www.3me.tudelft.nl/nmc

%% normal case
if nargin==0
    p=[];
    f=(0.1:0.1:20).';
    fxx=max(f);
elseif nargin==1
    f=(0.1:0.1:20).';
    fxx=max(f);
elseif nargin==2
    fxx=max(f);
end

%% check inputs, and give defaults
% See Table 2 in the article.
if ~isfield(p,'m');   p.m=2; end
if ~isfield(p,'bq');  p.bq=0; end
if ~isfield(p,'kq');  p.kq=0; end
if ~isfield(p,'b');   p.b=30; end
if ~isfield(p,'k');   p.k=400; end
if ~isfield(p,'kse'); p.kse=Inf; end
if ~isfield(p,'bc');  p.bc=[]; end
if ~isfield(p,'kc');  p.kc=[]; end
if ~isfield(p,'me');  p.me=1; end
if ~isfield(p,'be');  p.be=0; end
if ~isfield(p,'ke');  p.ke=0; end
if ~isfield(p,'F0');  p.F0=2.5; end
if ~isfield(p,'Bt');  p.Bt=0.7; end
if ~isfield(p,'Td');  p.Td=0.025; end
if ~isfield(p,'kp');  p.kp=0; end
if ~isfield(p,'kv');  p.kv=30; end
if ~isfield(p,'ka');  p.ka=0; end
if ~isfield(p,'kf');  p.kf=0; end

%% The model
H.f=f; % pass through the frequency vector

% s-vector (LaPlace operator)
s=2*pi*1j*f;
s2=-4*pi^2*f.^2; % j^2=-1

% Sub transfer functions, 
% See also Figures 2 and 3, and Appendix A, within the article
% The numbers between the brackets refer to the equations in the article
%
% inertial and passive properties [Eq.A1]
Hi_inv=p.m*s2+p.bq*s+p.kq;
% intrinsic muscle properties (co-contraction) [Eq.A3]
Hce=p.k+p.b*s;
% muscle activation dynamics [Eq.A4]
a1=1./( (2*pi*p.F0)^2 );
a2=2*p.Bt./(2*pi*p.F0);
Hact=1./(a1*s2+a2*s+1);
% muscle spindle [Eq.A6]
Hms=(p.kp+p.kv*s+p.ka*s2).*exp(-p.Td*s);
% Golgi tendon organ [Eq.A7]
Hgto=p.kf*exp(-p.Td*s);
% environment [Eq.A13]
He_inv=p.me*s2+p.be*s+p.ke;

% the filter made by the GTO [Eq.1]
Hfiltgto=1./(1+Hgto.*Hact);
% intrinsic and reflexive feedback [Eq.2]
Hfb=Hce+(Hms-Hce.*Hgto).*Hfiltgto.*Hact;
% filter made by the SE (X=>Xse) [Eq.3]
Hfiltse=1./(1+Hfb./p.kse);

% (un)comment the following equations if you want to
%H.Hfiltse=Hfiltse;
%H.Hfiltgto=Hfiltgto;
%H.Hfbint=Hce;
%H.Hfbref=(Hms-Hce.*Hgto).*Hfilt_gto.*Hact;

% joint admittance (F=>X) [Eq.4]
H.Hfx=1./(Hi_inv+Hfiltse.*Hfb);
% reflexive impedance (X=>A) [Eq.5]
H.Hxa=Hfiltse.*(Hms-Hce.*Hgto).*Hfiltgto;

% adding the contact dynamics
if (isempty(p.bc)~=1)&&(isempty(p.kc)~=1)
    Hc=p.bc*s+p.kc; %[Eq.A14]
    H.Hxa=H.Hxa.*( (H.Hfx.*Hc)./(1+Hc.*H.Hfx) ); %[Eq.A16]
    H.Hfx=H.Hfx+(1./Hc); %[Eq.A15]
end

% total admittance [Eq.7]
H.Hdx=(H.Hfx)./(He_inv.*H.Hfx+1);

%% Stability (Appendix B), open-loop transfer functions
% Beware the 'cut' is between muscle spindle and summation point (=motoneuron)
% grip is not included in the joint stability (off course it is in
% total stability)
Hint=1./(Hi_inv+1e-10); % +1e-10: trick to prevent 'divide by zero'
He=1./(He_inv+1e-10);

% Hol human [Eq.B5]
H.Holh=Hms.*Hfiltse.*Hint.*Hfiltgto.*Hact./(1+Hfiltse.*Hint.*Hfiltgto.*Hce);

% Hol total [Eq.B6]
if (isempty(p.bc)~=1)&&(isempty(p.kc)~=1)
    Hint2=(Hint-Hc.*He.*Hint)./(1-Hc.*He-Hc.*Hint);
else
    Hint2=1./(Hi_inv+He_inv+1e-10);
end
H.Holt=Hms.*Hfiltse.*Hint2.*Hfiltgto.*Hact./(1+Hfiltse.*Hint2.*Hfiltgto.*Hce);

%% check for unstability

H.unsh=0;
% unstability can only occur if real(H) becomes smaller than -1 (Nyquist!)
if min(real(H.Holh))<-1
    % solve the exception, i.e. starting on -real
    if (angle(H.Holh(1))>0.95*pi)||(angle(H.Holh(1))<-0.95*pi)
        H180=interp1(imag(H.Holh(1:4)),real(H.Holh(1:4)),0,[],'extrap');
        H.unsh=H.unsh+(H180<-1); % add 1 if unstability present
    end
    % normal case: find where H goes through -real
    a=sign(angle(H.Holh));
    nsign=find(a(1:end-1)~=a(2:end));
    if ~isempty(nsign)
        for loop=1:length(nsign)
            x=imag(H.Holh(nsign(loop):nsign(loop)+1));
            y=real(H.Holh(nsign(loop):nsign(loop)+1));
            H180=interp1(x,y,0);
            H.unsh=H.unsh+(H180<-1); % add 1 if unstability present
        end
    end
end

H.unst=0;
% unstability can only occur if real(H) becomes smaller than -1 (Nyquist!)
if min(real(H.Holt))<-1
    % solve the exception, i.e. starting on -real
    if (angle(H.Holt(1))>0.95*pi)||(angle(H.Holt(1))<-0.95*pi)
        H180=interp1(imag(H.Holt(1:4)),real(H.Holt(1:4)),0,[],'extrap');
        H.unst=H.unst+(H180<-1); % add 1 if unstability present
    end
    % normal case: find where H goes through -real
    a=sign(angle(H.Holt));
    nsign=find(a(1:end-1)~=a(2:end));
    if ~isempty(nsign)
        for loop=1:length(nsign)
            x=imag(H.Holt(nsign(loop):nsign(loop)+1));
            y=real(H.Holt(nsign(loop):nsign(loop)+1));
            H180=interp1(x,y,0);
            H.unst=H.unst+(H180<-1); % add 1 if unstability present
        end
    end
end

%% performance criteria [Eq.9]
[dum,n]=min(abs(f-fxx));
if H.unsh==0
    J.xh=sum(abs(H.Hfx(1:n).^2));
else
    J.xh=NaN;
end
if H.unst==0
    J.xt=sum(abs(H.Hdx(1:n).^2));
else
    J.xt=NaN;
end
