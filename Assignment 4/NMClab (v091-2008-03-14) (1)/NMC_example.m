%% example script to show how to call the function NMC_tf.m
%
% NMC_tf calculates the frequency response of a neuromusculoskeletal model.
%
% syntax: [H] = NMC_tf(p,f) or [H,p,J] = NMC_tf(p,f)
%
% The model is described in:
% "NMClab, a model to assess the contributions of muscle visco-elasticity
% and afferent feedback to joint dynamics."
% Schouten, Mugge, van der Helm. J Biomech, 2008, accepted.
%
% version 0.91: pre-release, February 29, 2008
% Copyright (C) 2007-2008, Alfred C. Schouten
% This software and model may be used, copied, or redistributed as long as
% it is not sold and this copyright notice is reproduced on each copy made.
% This model is provided as is, without any express or implied warranties
% whatsoever. 
%
% Please acknowledge the use of this model and software in any publications
% by refering to the article: 
%  "NMClab, a model to assess the contributions of muscle visco-elasticity
%  and afferent feedback to joint dynamics."
%  Schouten, Mugge, van der Helm. J Biomech, 2008, accepted.
%
% Alfred C. Schouten
% Delft Laboratory for NeuroMuscular Control
% Department of (Bio)Mechanical Engineering
% Delft University of Technology
%
% Mekelweg 2
% 2628 CD Delft
% The Netherlands
% e-mail: A.C.Schouten@tudelft.nl
% url:    www.3me.tudelft.nl/nmc

% p: parameter vector
%  default parameters (Table 2)
clear p
p.m=2;      % mass [kg]
p.b=30;     % muscle viscosity [Ns/m]
p.k=400;    % muscle elasticty
p.F0=2.5;   % bandwidth of the muscle activation dynamics [Hz]
p.Bt=0.7;   % relative damping of the muscle activation dynamics [-]
p.kse=10e3; % tendon elasticity [N/m]
p.kv=30;    % afferent feedback gain for stretch velocity [Ns/m]
p.Td=0.025; % neural time delay [s]

% f: frequency vector
f=(0.1:0.1:20); % [Hz]

% call the script
% H: structure with FRFs
%   e.g. H.Hfx is the FRF of the human joint
%        H.Hdx is the FRF of the combined system (joint with environment)
[H,p]=NMC_tf(p,f);

% plot the result in Bode diagrams
%  left: the mechanical admittance of the joint and the combined system
%  right: the reflexive impedance 
figure
subplot(221)
loglog(H.f,abs(H.Hfx)), hold on
loglog(H.f,abs(H.Hdx),'r--')
xlim([min(H.f) max(H.f)])
title('Mechanical Admittance (H_{fx})')
ylabel('gain [m/N]')
subplot(223)
semilogx(H.f,unwrap(angle(H.Hfx))*180/pi), hold on
semilogx(H.f,unwrap(angle(H.Hdx))*180/pi,'r--')
xlim([min(H.f) max(H.f)])
set(gca,'YTick',-360:90:360)
ylim([-180,0])
legend({'joint','total'})
ylabel('phase [\circ]')
xlabel('frequency [Hz]')

subplot(222)
loglog(H.f,abs(H.Hxa))
xlim([min(H.f) max(H.f)])
title('Reflexive Impedance (H_{xa})')
ylabel('gain [N/m]')
subplot(224)
semilogx(H.f,unwrap(angle(H.Hxa))*180/pi)
xlim([min(H.f) max(H.f)])
set(gca,'YTick',-360:90:360)
ylim([-180,180])
ylabel('phase [\circ]')
xlabel('frequency [Hz]')
