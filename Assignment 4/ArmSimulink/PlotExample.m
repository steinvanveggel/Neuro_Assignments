%% Plot ArmSimulink data

% after running ArmSimulink the follwing signals will exists in workspace
%
% time:     time vector
% x:        deviations (position)
% Fmuscle:  muscle (reaction-)force
% u:        neural input (=input from spinal cord motoneurons to muscle)
% ssu:      supraspinal input (=input from CNS to spinal cord)
% Fex:      external force at the hand/arm

figure
subplot(511), plot(time,Fex,'LineWidth',2), ylabel('external force [N]')
subplot(512), plot(time,ssu,'LineWidth',2), ylabel('supraspinal input [-]')
subplot(513), plot(time,u,'LineWidth',2), ylabel('neural input [-]')
subplot(514), plot(time,Fmuscle,'LineWidth',2), ylabel('muscle force [N]')
subplot(515), plot(time,x,'LineWidth',2), ylabel('deviations [m]')
xlabel('tijd [s]')

%% if the input is a chirp
ch=0;
if ch==1
    figure
    n1=find(time>0.5,1); % first value after 1 s (to improve figure readability)
    loglog(time(n1:end),abs(x(n1:end)),'LineWidth',2)
    ylabel('deviation [m]')
    xlabel('time [s]')
end
    